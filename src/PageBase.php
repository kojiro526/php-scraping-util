<?php
namespace Ssk526\PhpScrapingUtil;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

abstract class PageBase
{
	public $http_client = null;
	public $html = null;
	public $url = '';
	public $directories = array();
	public $request_params = array();
	
	private $client = null;
	private $parser = array();
	private $parser_callback = array();
	private $dispatch_cases = array();
	private $parser_default = null;
	private $parser_default_callback = null;
	private $pager_parser = array();
	private $pager_parser_default = null;
	private $before_filters = array();
	private $after_filters = array();
	private $next_page_parser_default = null;
	private $next_page_parser = array();
	
	private $is_pager_crawling = false;
	private $next_case = null;
	
	public $verbose = false;
	public $log_file = '';
	public $output_file = '';
	public $output_enc = 'SJIS';
	public $cmdargs = null;
	
	function __construct()
	{
		$this->initialize();
		$this->parser_default = function(){};
	}
	
	/**
	 * 派生クラスでオーバーライドし、append_parserなどの処理を記述する。
	 */
  	protected function initialize(){}
	
	public function test()
	{
		echo 'TEST';
	}
	
	/**
	 * ページ内の要素をスキャンして結果を返す。
	 * ページ内容を判定して、あらかじめappendしたクロージャーの処理を行う。
	 * ページネーションの探索が有効に設定されている場合は、次ページがある限りクロールする。
	 * 次ページのURLを取得するロジックは該当するメソッドをオーバーライドして実装する。
	 * コールバックを設定することで、ページ内のスキャン結果を引数として渡して処理を行うことができる。
	 * @param callback $func
	 */
	public function scan(callable $func=null)
	{
		$this->put_verbose("Scaning start.");
		
		$this->html = $this->get_html($this->url);
		$this->html = mb_convert_encoding($this->html, 'UTF-8', 'auto');
		
		$_call_func = null;
		$_call_case = null;
		
		/**
		 * caseが明示的に指定されている場合は該当のパーサを取得する。
		 */
		if(!empty($this->next_case))
		{
			$_call_case = $this->next_case;
			if(key_exists($_call_case, $this->parser))
			{
				$_call_func = $this->parser[$_call_case];
			}else{
				throw new \Exception("Parser '{$_call_case}' is not exists.");
			}
		}else{
		
			if(count($this->dispatch_cases)==0)
			{
				//ディスパッチ用の判定ロジックが登録されていない場合はデフォルトが実行される。
				$_call_func = $this->parser_default;
				$_call_case = 'default';
			}else{
				//ディスパッチ用の判定ロジックが登録されている場合は登録順に判定し、該当が無い場合はデフォルトが実行される。			
				$_call_case = $this->witch_case($this->html);
				if($_call_case==false)
				{
					$_call_func = $this->parser_default;
					$_call_case = 'default';
				}else{
					$_call_func = $this->parser[$_call_case];
				}
			}
		}
		$this->put_verbose('Detected call case :'.$_call_case);
		
		$this->html = $this->apply_before_filter($_call_case, $this->html);
		
		$_ret_array = array();
		$_ret_tmp = call_user_func($_call_func, $this->html);
		if(!empty($_ret_tmp))
		{
			$_ret_array = $_ret_tmp;
		}
		
		//ページャーのクロールが有効の場合はパーサを取得
		$_pager_parser = null;
		if($this->is_pager_crawling())
		{
			$this->put_verbose('Pager crawling is ON');
			if($_call_case == 'default')
			{
				$_pager_parser = $this->next_page_parser_default;
			}else{
				if(array_key_exists($_call_case, $this->next_page_parser))
				{
					$_pager_parser = $this->next_page_parser[$_call_case];
				}else{
					$_pager_parser = $this->next_page_parser_default;
				}
			}
		}
		
		//ページャーのクロールが有効の場合は2ページ目以降の処理を実行
		if($this->is_pager_crawling()&&!empty($_pager_parser))
		{
			$this->put_verbose('Pager crawling start.');
			
			$_next_page_link = call_user_func($_pager_parser, $this->html);
			while($_next_page_link!=false)
			{
				$this->put_verbose('Next page link = '.$_next_page_link['href']);
				sleep(1);
				
				$this->html = $this->get_html($_next_page_link['href']);
				$this->html = mb_convert_encoding($this->html, 'UTF-8', 'auto');

				$_ret_array_tmp = call_user_func($_call_func, $this->html);
				foreach($_ret_array_tmp as $_tmp)
				{
					array_push($_ret_array, $_tmp);
				}
				
				$_next_page_link = call_user_func($_pager_parser, $this->html);
			}
		}
		
		//アフターフィルターを実行
		$_ret_array = $this->apply_after_filter($_call_case, $_ret_array);
		
		//scanメソッドの引数として渡したコールバックを実行
		if($func!=null)
		{
			call_user_func($func, $_ret_array);
		}
		
		//appendしたコールバックを実行
		$_scan_callback = null;
		if($_call_case == 'default')
		{
			$_scan_callback = $this->parser_default_callback;
		}else{
			if(array_key_exists($_call_case, $this->parser_callback))
			{
				$_scan_callback = $this->parser_callback[$_call_case];
			}
		}
		if(!empty($_scan_callback))
		{
			call_user_func($_scan_callback, $_ret_array);
		}
		
		$this->put_verbose('Scaning end.');
		return $_ret_array;
	}
	
	/**
	 * スキャンした結果をフィルタするコールバックを設定する。
	 * @param string $key
	 * @param callback $func
	 */
	public function append_after_filter($key, $func)
	{
		$this->after_filters[$key] = $func;
	}

	/**
	 * GETしたHTMLに対してフィルタを実行する。
	 * 検索フォームなどでパラメータをPOSTする必要がある場合に、GETしたHTML
	 * @param string $key
	 * @param callback $func
	 */
	public function append_before_filter($key, $func)
	{
		$this->before_filters[$key] = $func;
	}
	
	/**
	 * HTMLをパースする処理をコールバックとして追加する。
	 * $keyとして'default'が渡された場合は、デフォルトケースとして登録する。
	 * @param string $key
	 * @param callback $func
	 */
	public function append_parser($key, $func)
	{
		if($key=='default')
		{
			$this->parser_default = $func;
		}else{
			$this->parser[$key] = $func;
		}
	}
	
	/**
	 * scanメソッド内のパース後に、取得した配列に対して行うコールバック処理を設定する。
	 * @param string $key
	 * @param callback $func
	 */
	public function append_scan_callback($key, $func)
	{
		if($key=='default')
		{
			$this->parser_default_callback = $func;
		}else{
			$this->parser_callback[$key] = $func;
		}
	}
	
	public function append_next_page_parser($key, $func)
	{
		if($key=='default')
		{
			$this->next_page_parser_default = $func;
		}else{
			$this->next_page_parser[$key] = $func;
		}
	}
	
	/**
	 * HTMLの内容を判別するための処理をコールバックとして追加する。
	 * @param string $key
	 * @param unknown $func
	 */
	public function append_case($key, $func)
	{
		$this->dispatch_cases[$key] = $func;
	}
	
	/**
	 * アフターフィルターを適用する。
	 * @param string $case
	 * @param array $array
	 */
	public function apply_after_filter($case, array $array)
	{
		if(array_key_exists($case, $this->after_filters))
		{
			$func = $this->after_filters[$case];
		}else{
			return $array;
		}
		return call_user_func($func, $array);
	}
	
	public function apply_before_filter($case, $html)
	{
		if(array_key_exists($case, $this->before_filters))
		{
			$func = $this->before_filters[$case];
		}else{
			return $html;
		}
		return call_user_func($func, $html);
	}
	
	/**
	 * HTMLからタイトル要素内のテキストを取得する。
	 * @param string $html
	 */
	public function get_title($html)
	{
		$crawler = new Crawler();
		$crawler->addHtmlContent($html);
		$title = $crawler->filter('title');
		if(empty($title->getNode(0)))
		{
			return null;
		}else{
			return $title->text();
		}
	}
	
	/**
	 * HttpClientをセットする。
	 * @param unknown $client
	 */
	public function set_client($client)
	{
		$this->client = $client;
	}
	
	/**
	 * ページャークロールをONにする。
	 */
	public function set_pager_crawling_on()
	{
		$this->is_pager_crawling = true;
	}

	/**
	 * ページャークロールをONにする。
	 */
	public function set_pager_crawling_off()
	{
		$this->is_pager_crawling = false;
	}
	
	/**
	 * 引数として渡されたHTMLをもとに、ページの内容を判定するキーを返す。
	 * あらかじめアペンドした処理のいずれにも該当しなかった場合はfalseを返す。
	 * @param string $html
	 * @return string | false
	 */
	public function witch_case($html)
	{
		foreach($this->dispatch_cases as $key => $func)
		{
			$case = call_user_func($func, $html);
			if($case==false)
			{
				continue;
			}else{
				return $key;
			}
		}
		return false;
	}
	
	/**
	 * リクエストヘッダに設定するパラメータをセットする。
	 * @param array $params
	 */
	public function set_request_params(array $params)
	{
		$this->request_params = $params;
	}
	
	/**
	 * Htmlをテキストとして取得する。
	 * 必要に応じてオーバーライドする。
	 */
	protected function get_html($url)
	{
		if(empty($this->http_client))
		{
			$_client = $this->get_http_client();
		}else{
			$_client = $this->http_client;
		}
		$_crawler = $_client->request('GET', $url);
		return $_crawler->html();
	}
	
	/**
	 * HTTPクライアントを生成する。
	 * あらかじめ設定したリクエストパラメータをクライアントに設定する。
	 * @return \Ssk526\PhpScrapingUtil\Client
	 */
	protected function get_http_client()
	{
		if(!empty($this->client))
		{
			return $this->client;
		}
		
		$client = new Client();
	
		$_client_params = array();
		$_client_params['verify'] = false;
		
		if(array_key_exists('proxy', $this->request_params))
		{
			$_client_params['proxy'] = $this->request_params['proxy'];
		}
		
		$client->setClient(new \GuzzleHttp\Client($_client_params));
		
		//以下のドキュメントを参考
		//http://docs.guzzlephp.org/en/latest/index.html
		//HTTPリクエストヘッダーを設定
		if(array_key_exists('headers', $this->request_params))
		{
			$_params_headers = $this->request_params['headers'];
			if(is_array($_params_headers))
			{
				foreach($_params_headers as $key => $val)
				{
					$client->setHeader($key, $val);
				}
			}else{
				trigger_error('You must set ARRAY to request headers.', E_USER_NOTICE);
			}
		}
	
		$this->http_client = $client;
		return $client;
	}

	protected function has_pagination()
	{
	
	}
	
	/**
	 * ページネーションをクローリングするかどうかを判定する。
	 */
	protected function is_pager_crawling()
	{
		return $this->is_pager_crawling;
	}
	
	/**
	 * バーバスモードの場合はコンソールに出力する。
	 * @param unknown $text
	 */
	private function put_verbose($text)
	{
		if($this->verbose)
		{
			echo $text."\n";
		}
	}
	
	/**
	 * 標準出力へ改行付きで文字列を出力する。
	 * 
	 * @param unknown $text
	 * @return string
	 */
	protected function puts($text)
	{
		if($this->output_enc == 'UTF-8')
		{
			echo $text."\n";
		}else{
			echo mb_convert_encoding($text, $this->output_enc, 'UTF-8')."\n";
		}
	}

	/**
	 * scanメソッドで使用するパーサを明示的に指定する。
	 * 
	 * append_caseで設定したコールバックによる判定を行わず、
	 * 明示的にcaseを指定する。
	 * 
	 * @param string $case ケース名
	 */
	public function set_next_case($case)
	{
		$this->next_case = $case;
	}
}