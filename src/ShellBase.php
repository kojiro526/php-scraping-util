<?php
namespace Ssk526\PhpScrapingUtil;

require_once './vendor/autoload.php';

use Console\CommandLine;

abstract class ShellBase
{
	public $command_parser = null;
	public $cmdargs = null;
	public $output_enc = 'SJIS';
	
	private $user_options = array();
	private $argv = null;
	
	function __construct($argv)
	{
		$this->argv = $argv;
		
	}
	
	abstract protected function main();
	
	/**
	 * 派生クラスのmainで設定したコマンドを実行する。
	 */
	public function execute()
	{
		try{
			$this->set_command_options();
		}catch(\Console_CommandLine_Exception $e){
			// コマンドオプションのパースで例外が発生した場合は終了する。
			die($e->getMessage());
		}
		
		$ret = $this->main();
		return $ret;
	}
	
	/**
	 * 派生クラスでコマンドラインオプションを追加する。
	 * @param array $array
	 */
	protected function append_user_option($name, array $array)
	{
		array_push($this->user_options, [$name, $array]);
	}

	/**
	 * ページオブジェクトにオプションをセットする。
	 * @param PageBase $page
	 */
	protected function build(PageBase $page)
	{
		$page->verbose = isset($this->cmdargs->options['verbose']) ? $this->cmdargs->options['verbose'] : false;
		$page->log_file = isset($this->cmdargs->options['log_file']) ? $this->cmdargs->options['log_file'] : '';
		$page->output_file = isset($this->cmdargs->options['output_file']) ? $this->cmdargs->options['output_file'] : '';
		if(!empty($this->cmdargs->options['output_enc']))
		{
			$page->output_enc = $this->cmdargs->options['output_enc'];
		}
		$page->cmdargs = $this->cmdargs;
		return $page;
	}
	
	/**
	 * 基本オプションおよびユーザ設定オプションをパースする。
	 */
	protected function set_command_options()
	{
		$this->command_parser = new  \Console_CommandLine();
		
		$this->command_parser->addOption('output_enc', [
				'short_name' => '-e',
				'long_name' => '--output-enc',
				'action' => 'StoreString',
				'help_name' => 'ENCODE',
				'description' => mb_convert_encoding('標準出力への出力時のエンコード', 'SJIS', 'UTF-8')
		]);
		
		$this->command_parser->addOption('output_file', [
				'short_name' => '-o',
				'long_name' => '--output-file',
				'action' => 'StoreString',
				'help_name' => 'FILE_PATH',
				'description' => mb_convert_encoding('出力先となるファイル', 'SJIS', 'UTF-8')
		]);
		
		$this->command_parser->addOption('log_file', [
				'short_name' => '-l',
				'long_name' => '--log-file',
				'action' => 'StoreString',
				'help_name' => 'FILE_PATH',
				'description' => mb_convert_encoding('ログ出力先となるファイル', 'SJIS', 'UTF-8')
		]);

		$this->command_parser->addOption('config_file', [
				'short_name' => '-c',
				'long_name' => '--config-file',
				'action' => 'StoreString',
				'help_name' => 'FILE_PATH',
				'description' => mb_convert_encoding('外部設定ファイル', 'SJIS', 'UTF-8')
		]);
		
		foreach($this->user_options as $option)
		{
			$this->command_parser->addOption($option[0], $option[1]);
		}
		
		$this->command_parser->addOption('verbose', [
				'long_name' => '--verbouse',
				'action' => 'StoreTrue',
				'description' => 'Verbose mode.'
		]);
		
		try{
			$this->cmdargs = $this->command_parser->parse(count($this->argv) ,$this->argv);
			//var_dump($_cmd_res);
		}catch(Exception $e){
			var_dump($e->getMessage());
			exit;
		}
	}
	
	/**
	 * 標準出力へ改行付きで文字列を出力する。
	 *
	 * @param unknown $text
	 * @return string
	 */
	protected function puts($text)
	{
		if($this->output_enc == 'UTF-8')
		{
			echo $text."\n";
		}else{
			echo mb_convert_encoding($text, $this->output_enc, 'UTF-8')."\n";
		}
	}	
}